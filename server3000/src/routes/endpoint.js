const { Router } = require('express');
const router = Router();


router.get('/semi1-8', async (req, res) => {
    res.status(200).json("error")
})

router.post('/suma', async (req, res) => {
    let num1=req.body.num1;
    let num2=req.body.num2;

    const suma=num1+num2

    res.status(200).json(suma)
})

router.post('/resta', async (req, res) => {
    let num1=req.body.num1;
    let num2=req.body.num2;

    const resta=num1-num2

    res.status(200).json(resta)
})

module.exports=router;